<?php
/**
 * Created by PhpStorm.
 * User: Da Xiong
 * Date: 2019/1/24
 * Time: 14:15
 */

namespace App\HttpController\Api;


use App\HttpController\BaseController;
use App\Model\ConfigModel;

class Config extends BaseController
{

    /**
     * 获取配置信息
     */
    public function index(){
        $request = $this->request();
        $name= $request->getRequestParam('name') ?? '';
        if(!$name){
            return $this->writeJson(1,'error');
        }
        $configModel = new ConfigModel();
        $result = $configModel->where('name',$name)->first();
        return $this->writeJson(0,$result);
    }
}
