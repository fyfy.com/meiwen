<?php

namespace App\HttpController\Api;


use App\HttpController\BaseController;
use App\Model\ArticlesModel;
use App\Model\FormidsModel;
use App\Model\UsersModel;
use App\Utility\Music;
use App\Utility\RedisTools;
use Carbon\Carbon;
use EasySwoole\EasySwoole\Logger;

class Index extends BaseController
{

    /**
     * 列表
     * @return bool|void
     * @throws \Throwable
     */
     public function index(){
        try{
            $request = $this->request();
            $params = $request->getRequestParam();
            $model = new ArticlesModel();
            $list = $model->getApiList($params);
            return $this->writeJson(0,$list);
        }catch (\Exception $e){
            return $this->writeJson(1,null,$e->getMessage());
        }
    }
    public function take(){
        try{
            $request = $this->request();
            $params = $request->getRequestParam();
            $model = new ArticlesModel();
            $list = $model->getApiListTake($params);
            return $this->writeJson(0,$list);
        }catch (\Exception $e){
            return $this->writeJson(1,null,$e->getMessage());
        }
    }


    /**
     * 获取详情
     */
    public function info(){
        try{
            $request = $this->request();
            $id = $request->getRequestParam('id') ??  0;
            $share = $request->getRequestParam('share') ?? null;
            $userId = $request->getRequestParam('user_id') ?? 0;

            if(empty($id)){
                return $this->writeJson(1,null,'参数不正确');
            }
            $model = new ArticlesModel();
            $info = $model->findData($id);
            if(empty($info)){
                return $this->writeJson(1,null,'抱歉，数据不存在！');
            }
            $model->updateViewData($id,$info,$share,$userId);
            $info['music'] = (new Music())->getMusic($info['music']);
            return $this->writeJson(0,$info);
        }catch (\Exception $e){
            return $this->writeJson(1,null,$e->getMessage());
        }
    }

    /**
     * 随机查询8条文章
     * @return bool
     * @throws \Throwable
     */
    public function list(){
        try{
            $model = new ArticlesModel();
            $list = $model->inRandomOrder()->select('id,thumb,title')->take(8)->getAll();
            return $this->writeJson(0,$list);
        }catch (\Exception $e){
            return $this->writeJson(1,null,$e->getMessage());
        }
    }


    /**
     * 收集模板消息的formid
     * @return bool
     * @throws \Throwable
     */
    public function formId(){
        try{
            $request = $this->request();
            $data = $request->getRequestParam();
            if($data['formid'] =='the formId is a mock one'){
                return $this->writeJson(1,null,'测试数据');

            }
            $umodel = new UsersModel();
            $user = $umodel->findData($data['openid']);
            if(!$user){
                return $this->writeJson(1,null,'用户数据非法');
            }
            $queue = new RedisTools('formid');
            $queue->push([
                'openid' => $data['openid'],
                'expire' => Carbon::now()->addDays(7)->timestamp,
                'formid' => $data['formid'],
                'name'   =>'chunqiu'
            ]);
            return $this->writeJson(0);
        }catch (\Exception $e){
            return $this->writeJson(1,null,$e->getMessage());
        }
    }





    /**
     * 随机查询3条文章用于大队长视频广告
     * @return bool
     * @throws \Throwable
     */
    public function daduizhang(){
        try{
            $model = new ArticlesModel();
            $list = $model->inRandomOrder()->where('hot',1)->select('id,thumb,title')->take(3)->getAll();
            return $this->writeJson(0,$list);
        }catch (\Exception $e){
            return $this->writeJson(1,null,$e->getMessage());
        }
    }
}
