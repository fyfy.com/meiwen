<?php

namespace App\HttpController\Admin;

use App\Model\ArticlesModel;
use App\Model\CountsModel;
use App\Model\UsersModel;
use Carbon\Carbon;

class Index extends Auth{



    public function index(){
        $userModel = new UsersModel();
//        //今日新增人数
//        $userNew = $userModel->whereDate('reg_at', Carbon::now()->toDateString())->count();
//        //今日活跃人数
//        $userLogin = $userModel->whereDate('login_at', Carbon::now()->toDateString())->count();
        $articleModel = new ArticlesModel();
        //今日发布文章数
        $articleNew = $articleModel->whereDate('created_at', Carbon::now()->toDateString())->count();
//        $countModel = new CountsModel();
//        //今日文章浏览数
//        $countView = $countModel->where('date', Carbon::now()->toDateString())->sum('view');
//        //今日有效分享数
//        $countShareIn = $countModel->where('date', Carbon::now()->toDateString())->sum('share_in');
//        //每个管理员当天数据的统计
//        $countCurrentDayNum = $countModel->getCurrentByAdmin();
//        //每个管理员当月数据的统计
//        $countCurrentMonthNum = $countModel->getCurrentByAdmin(2);
//        //每个管理员当月文章的统计
//        $countCurrentArticleNew = $articleModel->getCurrentByAdmin();
//        foreach ($countCurrentArticleNew as $key=>&$value){
//            foreach ($countCurrentMonthNum as $k=>$val){
//                if($value['userid'] == $val['admin_id']){
//                    $value['avg'] = round($val['views']/$value['nums'],2);
//                }
//            }
//        }
        $return = [
            'userNew' => $userNew,
            'userLogin' => $userLogin,
            'articleNew' => $articleNew,
//            'countView' => $countView,
//            'countShareIn' =>  $countShareIn,
//            'countCurrentDayNum' =>  $countCurrentDayNum,
//            'countCurrentMonthNum' =>  $countCurrentMonthNum,
//            'countCurrentArticleNew' => $countCurrentArticleNew
        ];
        return $this->writeJson(0,$return);
    }



}
