<?php


namespace App\HttpController\Admin;

use App\Model\RolesModel;
use Carbon\Carbon;

class Role extends Auth {

    /**
     * 列表
     * @return bool|void
     * @throws \Throwable
     */
    public function index(){
        try{
            $request = $this->request();
            $page = $request->getRequestParam('page') ?? 1;
            $model = new RolesModel();
            $list = $model->select('id,name,note,created_at')->orderBy('created_at')->paginate($page);
            return $this->writeJson(0,$list);
        }catch (\Exception $e){
            return $this->writeJson(1,null,$e->getMessage());
        }

    }

    /**
     * 更新
     * @return bool
     * @throws \EasySwoole\Mysqli\Exceptions\ConnectFail
     * @throws \EasySwoole\Mysqli\Exceptions\PrepareQueryFail
     * @throws \Throwable
     */
    public function update(){
        try{
            $request = $this->request();
            $data = $request->getRequestParam();
            $model = new RolesModel();
            if($data['id']){
                //修改
                $model->update($data);
                return $this->writeJson(0,null,'编辑成功');
            }else{
                //新增
                $data['created_at'] = Carbon::now();
                $model->create($data);
                return $this->writeJson(0,null,'新增成功');
            }
        }catch (\Exception $e){
            return $this->writeJson(1,null,$e->getMessage());
        }
    }

    /**
     * 删除
     * @return bool
     * @throws \Throwable
     */
    public function destroy(){
        try{
            $request = $this->request();
            $id = $request->getRequestParam('id');
            if(empty($id)){
                return $this->writeJson(1,null,'参数错误');
            }
            $model = new RolesModel();
            $res = $model->destroy($id);
            return $this->writeJson(0,$res);
        }catch (\Exception $e){
            return $this->writeJson(1,null,$e->getMessage());
        }
    }


    /**
     * 权限
     * @return bool
     * @throws \Throwable
     */
    public function access(){
        try{
            $request = $this->request();
            $id = $request->getRequestParam('id');
            if(empty($id)){
                return $this->writeJson(1,null,'参数错误');
            }
            $model = new RolesModel();
            $info = $model->select('rules')->find($id);
            return $this->writeJson(0,$info);
        }catch (\Exception $e){
            return $this->writeJson(1,null,$e->getMessage());
        }
    }

    /**
     * 更新权限
     * @return bool
     * @throws \EasySwoole\Mysqli\Exceptions\ConnectFail
     * @throws \EasySwoole\Mysqli\Exceptions\PrepareQueryFail
     * @throws \Throwable
     */
    public function accessUpdate(){
        try{
            $request = $this->request();
            $data = $request->getRequestParam();
            if(empty($data['id'])){
                return $this->writeJson(1,null,'参数错误');
            }
            $model = new RolesModel();
            $model->update($data);
            return $this->writeJson(0,null,'更新成功');
        }catch (\Exception $e){
            return $this->writeJson(1,null,$e->getMessage());
        }
    }

    /**
     * 获取所有角色
     * @return bool
     * @throws \Throwable
     */
    public function all(){
        try{
            $model = new RolesModel();
            $all = $model->select('id,name')->getAll();
            return $this->writeJson(0,$all);
        }catch (\Exception $e){
            return $this->writeJson(1,null,$e->getMessage());
        }
    }


}
