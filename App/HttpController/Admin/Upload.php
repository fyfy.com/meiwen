<?php
/**
 * Created by PhpStorm.
 * User: Da Xiong
 * Date: 2018/12/19
 * Time: 13:48
 */

namespace App\HttpController\Admin;


use App\HttpController\BaseController;
use App\Model\BaseModel;
use App\Utility\Filesystem;

class Upload extends BaseController
{

    /**
     * 单图片上传
     * @return bool
     * @throws \Exception
     */
    public function img(){
        try{
            $request = $this->request();
            $files = $request->getUploadedFile('pic');
            $return = (new Filesystem($files))->qiNiuAs();
            return $this->writeJson(0,$return);
        }catch (\Exception $e){
            return $this->writeJson(1,null,$e->getMessage());
        }
    }

    /**
     * 多图片上传
     * @return bool
     */
    public function imgs(){
        try{
            $request = $this->request();
            $files = $request->getUploadedFile('pic');
            $return = [];
            if(is_array($files)){
                foreach ($files as $file){
                    $return[] = (new Filesystem($file))->qiNiuAs();
                }
            }else{
                $return[] = (new Filesystem($files))->qiNiuAs();
            }
            return $this->writeJson(0,$return);
        }catch (\Exception $e){
            return $this->writeJson(1,null,$e->getMessage());
        }
    }
}
