<?php
/**
 * Created by PhpStorm.
 * User: Da Xiong
 * Date: 2018/12/19
 * Time: 17:31
 */

namespace App\HttpController\Admin;


use App\Model\Counts\CountsBean;
use App\Model\Counts\CountsModel;
use Carbon\Carbon;

class Count extends Auth
{
    public function index(){
        $request = $this->request();
        $data = $request->getRequestParam();
        $page = $data['page'] ?? 1;

        $beanData = [
            'start_time' => Carbon::parse('2016-10-15')->toDateTimeString(),
            'end_time' => Carbon::now(),
        ];
        if($data['art_id']){
            $beanData['art_id'] = $data['art_id'];
        }
        if(isset($data['start'])){
            $beanData['start_time'] = $data['start'];
        }
        if(isset($data['end'])){
            $beanData['end_time']  = $data['end'];
        }
        $bean = new CountsBean($beanData);
        $model = new CountsModel();
        $list = $model->paginate($page,$bean);
        return $this->writeJson(0,$list);
    }
}