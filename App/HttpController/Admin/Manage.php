<?php

namespace App\HttpController\Admin;

use App\Model\AdminsModel;
use Carbon\Carbon;

class Manage extends Auth {

    /**
     * 列表
     * @return bool|void
     * @throws \Throwable
     */
    public function index(){
        try{
            $request = $this->request();
            $page = $request->getRequestParam('page') ?? 1;
            $model = new AdminsModel();
            $list = $model->select('id,username,nickname,role_id,created_at,login_at')->orderBy('created_at')->paginate($page);
            return $this->writeJson(0,$list);
        }catch (\Exception $e){
            return $this->writeJson(1,null,$e->getMessage());
        }
    }

    /**
     * 更新
     * @return bool
     * @throws \Throwable
     */
    public function update(){
        try{
            $request = $this->request();
            $data = $request->getRequestParam();
            $model = new AdminsModel();
            if($data['id']){
                //修改
                if($data['password']){
                    $data['password'] = password_hash($data['password'],PASSWORD_DEFAULT);
                }else{
                    unset($data['password']);
                }
                $model->update($data);
                return $this->writeJson(0,null,'编辑成功');
            }else{
                //新增
                $data['password'] = $data['password'] ?? '123456';
                $data['password'] = password_hash($data['password'],PASSWORD_DEFAULT);
                $data['created_at'] = Carbon::now();
                $model->create($data);
                return $this->writeJson(0,null,'新增成功');
            }
        }catch (\Exception $e){
            return $this->writeJson(1,null,$e->getMessage());
        }
    }

    /**
     * 获取登录用户信息
     * @return bool
     * @throws \Throwable
     */
    public function info(){
        try{
            $model = new AdminsModel();
            $user = $model->user($this->token);
            return $this->writeJson(0,$user);
        }catch (\Exception $e){
            return $this->writeJson(1,null,$e->getMessage());
        }

    }

    /**
     * 删除
     * @return bool
     * @throws \Throwable
     */
    public function destroy(){
        try{
            $request = $this->request();
            $id = $request->getRequestParam('id') ?? 0;
            if(empty($id)){
                return $this->writeJson(1,null,'参数错误');
            }
            $model = new AdminsModel();
            $model->destroy($id);
            return $this->writeJson(0);
        }catch (\Exception $e){
            return $this->writeJson(1,null,$e->getMessage());
        }
    }


    /**
     * 获取所有管理员
     * @return bool
     * @throws \Throwable
     */
    public function all(){
        try{
            $model = new AdminsModel();
            $all = $model->select('id,nickname')->getAll();
            $new = [];
            foreach ($all as $item){
                $new[$item['id']] = $item['nickname'];
            }
            return $this->writeJson(0,$new);
        }catch (\Exception $e){
            return $this->writeJson(1,null,$e->getMessage());
        }
    }




}
