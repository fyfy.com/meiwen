<?php


namespace App\HttpController\Admin;

use App\Model\UsersModel;

class User extends Auth {

    public function index(){
        try{
            $request = $this->request();
            $page = $request->getRequestParam('page') ?? 1;
            $model = new UsersModel();
            $list = $model->select('id,openid,reg_at,login_at')->orderBy('reg_at')->paginate($page);
            return $this->writeJson(0,$list);
        }catch (\Exception $e){
            return $this->writeJson(1,null, $e->getMessage());
        }
    }


}
