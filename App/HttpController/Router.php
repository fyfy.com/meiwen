<?php

namespace App\HttpController;

use EasySwoole\Http\AbstractInterface\AbstractRouter;
use EasySwoole\Http\Message\Status;
use FastRoute\RouteCollector;
use EasySwoole\Http\Response;
use EasySwoole\Http\Request;
use EasySwoole\EasySwoole\Logger;

class Router extends AbstractRouter{
    public function initialize(RouteCollector $routeCollector){
        //开启全局模式拦截,局模式拦截下,路由将只匹配Router.php中的控制器方法响应,将不会执行框架的默认解析
        //不开启在拦截的状态下依旧会执行框架的默认解析
        $this->setGlobalMode(true);
//        $this->setMethodNotAllowCallBack(function (Request $request,Response $response){
//            $response->withStatus(Status::CODE_METHOD_NOT_ALLOWED);
//            $response->write('Sorry,the method not allowed.');
//        });
        $this->setRouterNotFoundCallBack(function (Request $request,Response $response){
            $response->withStatus(Status::CODE_NOT_FOUND);
            $response->write('Sorry, the page you are looking for could not be found.');
        });

        $routeCollector->get ('/wechat', '/Wechat/service');
        $routeCollector->post('/wechat', '/Wechat/service');

//        $this->addAdminRouter($routeCollector);
        $routeCollector->get('/', '/Home/Index');
        $routeCollector->post('/login', '/Home/Login');


        //API 小程序
        $routeCollector->post('/api/login', '/Api/Login');//登陆提交
        $routeCollector->get('/api/article', '/Api/Index');//首页列表
        $routeCollector->get('/api/article/take', '/Api/Index/take');//首页列表(不分页)

        $routeCollector->get('/api/article/info', '/Api/Index/info');//获取文章详情
        $routeCollector->get('/api/article/share', '/Api/Index/share');//分享提交
        $routeCollector->get('/api/article/list', '/Api/Index/list');//随机推荐
        $routeCollector->get('/api/article/daduizhang', '/Api/Index/daduizhang');//大队长广告
        $routeCollector->post('/api/form/id', '/Api/Index/formId');//收集FormId
        $routeCollector->get('/api/config', '/Api/Config/index');//获取公众号配置链接


        //管理后台
        $routeCollector->post('/admin/login', '/Admin/Login');//登陆提交
        $routeCollector->get('/admin/captcha', '/Admin/Login/captcha');//验证码

        $routeCollector->get('/admin', '/Admin/Index');//首页


        $routeCollector->get('/admin/manage', '/Admin/Manage');//管理员列表
        $routeCollector->get('/admin/manage/info', '/Admin/Manage/info');//管理员详情
        $routeCollector->post('/admin/manage/update', '/Admin/Manage/update');//管理员更新
        $routeCollector->get('/admin/manage/destroy', '/Admin/Manage/destroy');//管理员删除
        $routeCollector->get('/admin/manage/all', '/Admin/Manage/all');//所有管理员

        $routeCollector->get('/admin/role', '/Admin/Role');//角色列表
        $routeCollector->post('/admin/role/update', '/Admin/Role/update');//角色更新
        $routeCollector->get('/admin/role/destroy', '/Admin/Role/destroy');//角色删除
        $routeCollector->get('/admin/role/access', '/Admin/Role/access');//角色权限
        $routeCollector->post('/admin/role/access/update', '/Admin/Role/accessUpdate');//角色权限更新
        $routeCollector->get('/admin/role/all', '/Admin/Role/all');//全部角色

        //分类管理
        $routeCollector->get('/admin/category', '/Admin/Category');//分类管理列表
        $routeCollector->post('/admin/category/update', '/Admin/Category/update');//更新分类管理信息
        $routeCollector->delete('/admin/category/destory', '/Admin/Category/destroy');//删除分类管理信息
        $routeCollector->get('/admin/category/all', '/Admin/Category/all');//全部分类

        //上传
        $routeCollector->post('/admin/upload/img', '/Admin/Upload/img');//图片上传
        $routeCollector->post('/admin/upload/imgs', '/Admin/Upload/imgs');//图片上传

        //背景图管理
        $routeCollector->get('/admin/back', '/Admin/Back');//背景图列表
        $routeCollector->post('/admin/back/update', '/Admin/Back/update');//更新背景图信息
        $routeCollector->delete('/admin/back/destory', '/Admin/Back/destroy');//删除背景图信息
        $routeCollector->get('/admin/back/all', '/Admin/Back/all');//全部背景图

        //文章管理
        $routeCollector->get('/admin/article', '/Admin/Article');//文章列表
        $routeCollector->get('/admin/article/info', '/Admin/Article/info');//文章详情
        $routeCollector->post('/admin/article/update', '/Admin/Article/update');//更新文章信息
        $routeCollector->delete('/admin/article/destory', '/Admin/Article/destroy');//删除文章信息
        $routeCollector->post('/admin/article/push', '/Admin/Article/push');//预览，模板消息推送
        $routeCollector->delete('/admin/article/destory_all', '/Admin/Article/destroy_all');//批量删除文章信息
        //统计管理
        $routeCollector->get('/admin/count', '/Admin/Count');//统计列表

        //会员管理
        $routeCollector->get('/admin/user', '/Admin/User');//用户列表

        //模板消息
        $routeCollector->get('/admin/message', '/Admin/Message');//模板列表
        $routeCollector->post('/admin/message/update', '/Admin/Message/update');//模板消息编辑
        $routeCollector->post('/admin/message/push',   '/Admin/Message/push');//模板消息推送
        $routeCollector->get('/admin/message/token',  '/Admin/Message/token');//模板消息推送重置token
        $routeCollector->get('/admin/message/record', '/Admin/Message/record');//模板列表

        //配置管理
        $routeCollector->get('/admin/config', '/Admin/Config/index');// 获取配置信息
        $routeCollector->post('/admin/config/update', '/Admin/Config/update');// 更新配置信息
    }
}
