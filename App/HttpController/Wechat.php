<?php

namespace App\HttpController;

use App\Utility\MiniProgram;
use EasySwoole\EasySwoole\Logger;

class Wechat extends BaseController{

    public function service(){
        $request = $this->request();
        $params = $request->getRequestParam();
        $wechat = new MiniProgram();
        if($request->getMethod() == 'GET'){
            //微信接入
            $str = $wechat->service($params);
            $this->response()->withHeader('Content-Type','text');
            return $this->response()->write($str);
        }
        if($request->getMethod() == 'POST'){
            //该方法用于获取以非form-data或x-www-form-urlenceded编码格式POST提交的原始数据，相当于PHP中的$HTTP_RAW_POST_DATA。
            //获得raw内容
            $message = $wechat->getRaw($request->getBody());
            if(isset($message['MsgType'])){
                switch ($message['MsgType']){
                    case 'event':
                        switch ($message['Event']) {
                            case 'subscribe'://公众号关注
                                break;
                            case 'unsubscribe'://公众号取消关注
                                break;
                            case 'user_enter_tempsession'://小程序进入客服消息
                                break;
                            default://公众号菜单点击
                                break;
                        }
                        break;
                    case 'text':
                        //文字
                        $msg = [
                            'openid' => $message['FromUserName'],
                            'type'   => 'text',
                            'content'=> '<a data-miniprogram-appid="wxf240c2390d59a466" data-miniprogram-path="pages/index/index"> 点击跳小程序</a>',
                        ];
                        return $wechat->sendCustomerMessage($msg);
                        break;

                }
            }
        }



    }

}
