<?php

namespace App\HttpController\Home;

use App\HttpController\BaseController;
use App\Utility\Token;
use App\Utility\MiniProgram;
use EasySwoole\EasySwoole\Logger;
use EasySwoole\Http\Message\Status;
use EasySwoole\Http\Response;
use EasySwoole\Http\Request;

class Login extends BaseController {

    public function index(){
        //  getQueryParam
        $request = $this->request();
        $code = $request->getRequestParam('code');
        $mini = new MiniProgram();
        $res = $mini->session($code);
//        $token = Token::sessionEncrypt($code);
        return $this->writeJson(200,$res,'SECCUSS');
    }
}

