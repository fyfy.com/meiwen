<?php

namespace App\HttpController;

use EasySwoole\Http\AbstractInterface\Controller;

abstract class BaseController extends Controller{

    public function index() {
        $this->actionNotFound('index');
    }

    /**
     * 重置writeJson 方法
     * @param int $statusCode 0成功 1失败
     * @param null $result 结果
     * @param null $msg 消息提示
     * @return bool
     */
    protected function writeJson($statusCode = 0,$result = null,$msg = null){
        if(!$this->response()->isEndResponse()){
            $data = [
                "code"  => $statusCode,
                "data"  => $result,
                "msg"   => $msg ?? 'SUCCESS'
            ];
            $this->response()->write(json_encode($data,JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES));
            $this->response()->withHeader('Content-type','application/json;charset=utf-8');
            $this->response()->withStatus(200);
            return true;
        }else{
            return false;
        }
    }

}
