<?php

namespace App\Process;

use App\Model\FormidsChunqiuModel;
use App\Model\FormidsModel;
use App\Utility\RedisTools;
use EasySwoole\EasySwoole\Logger;
use EasySwoole\EasySwoole\Swoole\Process\AbstractProcess;

class FormIdPush extends AbstractProcess{
    private $isRun = false;
    public function run($arg){
        //定时500ms检测有没有任务，有的话就while死循环执行
        $this->addTick(1000,function (){
            if(!$this->isRun){
                $this->isRun = true;
                go(function (){
                    while (true){
                        try{
                            $model = new FormidsModel();
                            $list = $model->take(3000)->orderBy('id','ASC')->getAll();
                            $queue = new RedisTools('formid');
                            if(count($list)){
                                foreach ($list as $item){
                                    $queue->push([
                                        'openid' => $item['openid'],
                                        'expire' => $item['expires_at'],
                                        'formid' => $item['formid'],
                                        'name'   => 'chunqiu'
                                    ]);
                                    Logger::getInstance()->log($item['id'],'limit');
                                    $model->destroy($item['id']);
                                }
                                unset($model);
                            }else{
                                break;
                            }
                            sleep(10);
                            unset($queue);
                        }catch (\Throwable $throwable){
                            break;
                        }
                    }
                    $this->isRun = false;
                });
            }
        });
    }

    public function onShutDown()
    {
        // TODO: Implement onShutDown() method.
    }

    public function onReceive(string $str, ...$args)
    {
        // TODO: Implement onReceive() method.
    }
}
