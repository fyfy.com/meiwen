<?php

namespace App\Process;

use App\Model\FormidsModel;
use App\Model\RecordsModel;
use App\Utility\RedisTools;
use Carbon\Carbon;
use EasySwoole\EasySwoole\Logger;
use EasySwoole\EasySwoole\Swoole\Process\AbstractProcess;
use Swoole\Process;

class MessageCreate extends AbstractProcess
{
    private $isRun = false;
    public function run(Process $process){
        /*
         * 举例，消费redis中的队列数据
         * 定时500ms检测有没有任务，有的话就while死循环执行
         */
        $this->addTick(100,function (){
            if(!$this->isRun){
                $this->isRun = true;
                $queue = new RedisTools('message-create');
                \Co::create(function () use ($queue){
                    while (true){
                        try{
                            //lPop 乱序 rPop先进先出循序
                            $task = $queue->rPop();
                            if($task){
                                $queue_push = new RedisTools('message-push');
                                foreach ($task['item'] as $item){
                                    $item['record_id'] = $task['record_id'];
                                    $item['template_id'] = $task['template_id'];
                                    $queue_push->lPush($item);
                                }
                                unset($queue_push);
                            }else{
                                break;
                            }
                            unset($task);
                        }catch (\Throwable $throwable){
                            break;
                        }
                    }
                    $this->isRun = false;
                });
                unset($queue);
            }
        });
    }

    public function onShutDown()
    {
        // TODO: Implement onShutDown() method.
    }

    public function onReceive(string $str, ...$args)
    {
        // TODO: Implement onReceive() method.
    }
}
