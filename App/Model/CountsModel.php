<?php

namespace App\Model;

use Carbon\Carbon;
use EasySwoole\EasySwoole\Logger;

/**
 * 统计表
 * Class CountsModel
 * @package App\Model\Counts
 */
class CountsModel extends BaseModel{

    protected $table = 'counts_chunqiu';
    protected $fillable = [
        'art_id','view','id','admin_id','share_in','date'
    ];

    /**
     * 获取当天记录
     * @param $id
     * @return \EasySwoole\Mysqli\Mysqli|mixed|null
     * @throws \EasySwoole\Mysqli\Exceptions\ConnectFail
     * @throws \EasySwoole\Mysqli\Exceptions\PrepareQueryFail
     * @throws \Throwable
     */
    public function getCurrentDateInfo($id){
        $date = date('Y-m-d',time());
        $this->db->where('art_id',$id)->where('date',$date);
        return $this->first();
    }

    /**
     * 按用户分组统计
     * @param int $days $days>1的时候代表查询当月，1查询的是当天
     * @return mixed
     * @throws \EasySwoole\Mysqli\Exceptions\ConnectFail
     * @throws \EasySwoole\Mysqli\Exceptions\PrepareQueryFail
     */
    public function getCurrentByAdmin($days = 1){
        $time = time();
        $year = date('Y',$time);
        $month = date('m',$time);
        $date = Carbon::now()->toDateString();
        $date1 = Carbon::create($year,$month,1)->toDateString();
        if($days > 1){
            $result = $this->db->rawQuery('select sum(`view`) as views,sum(`share_in`) as shareins,admin_id from `'.$this->table.'` where date>="'.$date1.'" and date<="'.$date.'" group by admin_id order by views desc',[]);
        }else{
            $result = $this->db->rawQuery('select sum(`view`) as views,sum(`share_in`) as shareins,admin_id from `'.$this->table.'` where date="'.$date.'" group by admin_id order by views desc',[]);
        }
        return $result;
    }
}
