<?php

namespace App\Model;

use Carbon\Carbon;
use EasySwoole\EasySwoole\Logger;

class ArticlesModel extends BaseModel{

    protected $table = 'articles_chunqiu';
    protected $fillable = [
        'id','cateid','title','thumb','music_name','music','created_at','userid','title_color',
        'share','view','content','backimg','hot','lock','is_link','link_url'
    ];

    /**
     *  获取API文章列表
     * @param null $map
     * @return array
     * @throws \EasySwoole\Mysqli\Exceptions\ConnectFail
     * @throws \EasySwoole\Mysqli\Exceptions\Option
     * @throws \EasySwoole\Mysqli\Exceptions\OrderByFail
     * @throws \EasySwoole\Mysqli\Exceptions\PrepareQueryFail
     * @throws \Throwable
     */
    public function getApiList($params = null) {
        if($params){
            if(isset($params['hot']) && $params['hot']){
                $this->db->where('hot',$params['hot']);
            }
        }
        $this->db->where('status',1);
        $params['page'] = $params['page'] ?? 1;
        return $this->select('id,thumb,title')->orderBy('created_at')->paginate($params['page'],10);
    }
    public function getApiListTake($params = null) {
        if($params){
            if(isset($params['hot']) && $params['hot']){
                $this->db->where('hot',$params['hot']);
            }
        }
        $this->db->where('status',1);
        return $this->select('id,thumb,title')->inRandomOrder()->orderBy('created_at')->take(50)->getAll();
    }

    /**
     *  获取Admin文章列表
     * @param null $map
     * @return array
     * @throws \EasySwoole\Mysqli\Exceptions\ConnectFail
     * @throws \EasySwoole\Mysqli\Exceptions\Option
     * @throws \EasySwoole\Mysqli\Exceptions\OrderByFail
     * @throws \EasySwoole\Mysqli\Exceptions\PrepareQueryFail
     * @throws \Throwable
     */
    public function getAdminList($params = null) {
        switch ($params['sort']) {
            case 1:
                $sort = 'view';
                break;
            case 2:
                $sort = 'share';
                break;
            default:
                $sort = 'id';
                break;
        }
        if($params){
            if(isset($params['title']) && $params['title']){
                $this->db->whereLike('title','%'.$params['title'].'%');
            }
            if(isset($params['userid']) && $params['userid']){
                $this->where('userid',$params['userid']);
            }
            if(isset($params['hot']) && $params['hot'] != ''){
                $this->where('articles_chunqiu.hot',$params['hot']);
            }
            if(isset($params['lock']) && $params['lock'] != ''){
                $this->where('articles_chunqiu.lock',$params['lock']);
            }
            /**
             * Column 'created_at' in where clause is ambiguous query:
             * SELECT SQL_CALC_FOUND_ROWS articles_chunqiu.id,cateid,title,articles_chunqiu.thumb,music_name,articles_chunqiu.created_at,userid,articles_chunqiu.share,articles_chunqiu.view,hot,lock,c.name,a.nickname FROM articles RIGHT JOIN `categorys` as c on c.id = articles_chunqiu.cateid INNER JOIN `admins` as a on a.id = articles_chunqiu.userid WHERE created_at >= ? AND created_at <= ? ORDER BY articles_chunqiu.id DESC LIMIT 0, 10
             */
            if(isset($params['start'])){
                $this->where('articles_chunqiu.created_at',Carbon::parse($params['start'])->toDateString(),'>=');
            }
            if(isset($params['end'])){
                $this->where('articles_chunqiu.created_at',Carbon::parse($params['end'])->addDays(1)->toDateString(),'<=');
            }
        }
        $params['page'] = $params['page'] ?? 1;
        $this->db->join('`categorys` as c', 'c.id = '.$this->table.'.cateid','RIGHT')
            ->join('`admins` as a', 'a.id = '.$this->table.'.userid','INNER');
        $columns = 'articles_chunqiu.id,cateid,title,articles_chunqiu.thumb,music_name,articles_chunqiu.created_at,userid,articles_chunqiu.share,articles_chunqiu.view,articles_chunqiu.hot,articles_chunqiu.lock,c.name,a.nickname';
        return $this->select($columns)->orderBy('articles_chunqiu.'.$sort)->paginate($params['page']);
    }

    /**
     * 文章详情（api）
     * @param $article_id
     * @return \EasySwoole\Mysqli\Mysqli|mixed|null
     * @throws \EasySwoole\Mysqli\Exceptions\ConnectFail
     * @throws \EasySwoole\Mysqli\Exceptions\JoinFail
     * @throws \EasySwoole\Mysqli\Exceptions\PrepareQueryFail
     * @throws \Throwable
     */
    public function findData($article_id) {
        $this->db->join('`categorys` as a', 'a.id = '.$this->table.'.cateid','RIGHT');
        $columns = 'articles_chunqiu.id,cateid,userid,title,title_color,articles_chunqiu.thumb,music,articles_chunqiu.created_at,content,backimg,a.name as username,a.thumb as avatar';
        return $this->select($columns)->find($article_id);
    }


    public function updateViewData($id,$info,$share,$userId){
        $this->db->startTransaction();
        try{
            $countModel = new CountsModel();
            $countInfo = $countModel->getCurrentDateInfo($id);
            if($countInfo){
                //判断是否有效用户
                if($share && $userId>0){
                    $countLogModel = new CountsLogModel();
                    //判断是否记录当前用户当日首次分享进入
                    if(!$countLogModel->getCurrentLogDateInfo($countInfo['id'],$userId)){
                        $countLogModel->create(['count_id'=>$countInfo['id'],'user_id'=>$userId,'created_at'=>Carbon::now()]);
                        //当前用户当日首次分享记录有效分享次数+1
                        $countModel->inc($countInfo['id'],'share_in');
                        $this->inc($id,'share');
                    };
                }
                //更新阅读量
                $countModel->inc($countInfo['id'],'view');
            }else{
                //新增
                $count = [
                    'art_id' => $id,
                    'admin_id' => $info['userid'],
                    'share_in' => $share ? 1 : 0,
                    'view' => 1,
                    'date' => date('Y-m-d',time()),
                ];
                $countModel->create($count);
            }

            //增加当前文章阅读量
            $this->inc($id,'view');
            $this->db->commit();
        }catch (\Exception $e){
            $this->db->rollback();
            Logger::getInstance()->log($e->getMessage(),'database');
        }
        return true;
    }

    public function getCurrentByAdmin(){
        $time = time();
        $year = date('Y',$time);
        $month = date('m',$time);
        $date = Carbon::now()->toDateString();
        $date1 = Carbon::create($year,$month,1)->toDateString();
        $result = $this->db->rawQuery('select count(`id`) as nums,userid from `'.$this->table.'` where DATE_FORMAT(`created_at`,"%Y-%m-%d")>="'.$date1.'" and DATE_FORMAT(`created_at`,"%Y-%m-%d")<="'.$date.'" group by userid order by nums desc',[]);
        return $result;
    }



}
