<?php
/**
 * Created by PhpStorm.
 * User: Da Xiong
 * Date: 2019/1/10
 * Time: 16:37
 */

namespace App\Model;


class CountsLogModel extends BaseModel
{
    protected $table = 'counts_log';
    protected $fillable = [
        'id','count_id','user_id','created_at'
    ];
    public function getCurrentLogDateInfo($count_id,$user_id){
        $this->db->where('count_id',$count_id)->where('user_id',$user_id);
        return $this->first();
    }
}
