<?php

namespace App\Model;

class CategorysModel extends BaseModel{

    protected $table = 'categorys';
    protected $fillable = [
        'id','name','thumb','created_at'
    ];

}
