<?php

namespace App\Model;

use App\Utility\Token;

class AdminsModel extends BaseModel{

    protected $table = 'admins';

    protected $fillable = [
        'id','username','nickname','password','role_id','created_at','login_at'
    ];

    /**
     * 判断用户
     * @param $loginData
     * @return bool
     * @throws \EasySwoole\Mysqli\Exceptions\ConnectFail
     * @throws \EasySwoole\Mysqli\Exceptions\PrepareQueryFail
     * @throws \Throwable
     */
    public function check($loginData){
        $user = $this->db->where('username', $loginData['username'])->getOne($this->table);
        if($user && password_verify($loginData['password'],$user['password'])){
            return $user;
        }
        return false;
    }

    /**
     * 获取登陆用户信息
     * @param $token
     * @return bool|null
     * @throws \EasySwoole\Mysqli\Exceptions\ConnectFail
     * @throws \EasySwoole\Mysqli\Exceptions\PrepareQueryFail
     * @throws \Throwable
     */
    public function user($token){
        $username = Token::sessionCheckToken($token);
        if($username === false){
            return false;
        }
        return $this->db->where('username', $username)->getOne($this->table,'id,nickname,role_id,login_at,user_id');
    }













}
