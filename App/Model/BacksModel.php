<?php

namespace App\Model;

class BacksModel extends BaseModel{

    protected $table = 'backs';
    protected $fillable = [
        'id','img','created_at','title'
    ];


}
