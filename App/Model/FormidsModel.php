<?php

namespace App\Model;

class FormidsModel extends BaseModel{

    protected $table = 'formids';
    protected $fillable = [
        'id','formid','userid','openid','created_at','expires_at'
    ];



    public function clear(){
        return $this->db->where('expires_at',time(),'<=')->delete($this->table);
    }

}
