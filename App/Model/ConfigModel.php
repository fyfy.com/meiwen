<?php
/**
 * Created by PhpStorm.
 * User: Da Xiong
 * Date: 2019/1/24
 * Time: 14:11
 */

namespace App\Model;


class ConfigModel extends BaseModel
{
    protected $table = 'config';
    protected $fillable = [
        'id','name','title','value','option','created_at','updated_at'
    ];

}
