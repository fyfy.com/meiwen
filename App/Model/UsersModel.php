<?php

namespace App\Model;


use Carbon\Carbon;

class UsersModel extends BaseModel {

    protected $table = 'users';

    protected $fillable = [
        'id','openid','reg_at','login_at'
    ];

    /**
     * 根据openid查询一条用户数据
     * @param $openid
     * @return UsersBean|null
     * @throws \EasySwoole\Mysqli\Exceptions\ConnectFail
     * @throws \EasySwoole\Mysqli\Exceptions\PrepareQueryFail
     * @throws \Throwable
     */
    public function findData($openid){
        $this->db->where('openid', $openid);
        return $this->first();
    }




















}
