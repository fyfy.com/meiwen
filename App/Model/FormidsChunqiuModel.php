<?php

namespace App\Model;

class FormidsChunqiuModel extends BaseModel{

    protected $table = 'formids_chunqiu';
    protected $fillable = [
        'id','formid','openid','expires_at'
    ];



    public function clear(){
        return $this->db->where('expires_at',time(),'<=')->delete($this->table);
    }

}
