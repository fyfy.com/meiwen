<?php

namespace App\Utility\Pool;


use EasySwoole\Component\Pool\AbstractPool;
use EasySwoole\EasySwoole\Config;

class RedisPool extends AbstractPool{
    protected function createObject(){
        // TODO: Implement createObject() method.
        $redis = new RedisObject();
        $conf = Config::getInstance()->getConf('REDIS');
        if( $redis->connect($conf['host'],$conf['port'])){
            if(!empty($conf['password'])){
                $redis->auth($conf['password']);
            }
            if (!empty($conf['select'])) {
                $redis->select($conf['select']);
            }
            return $redis;
        }else{
            return null;
        }
    }
}
