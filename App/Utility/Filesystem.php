<?php

namespace App\Utility;

use EasySwoole\Utility\File;
use EasySwoole\EasySwoole\Config;
use Qiniu\Storage\UploadManager;
use Qiniu\Auth;

class Filesystem{

    protected $files;

    public function __construct($files = null){
        $this->files = $files;
    }

    /**
     * 保存文件到指定文件夹
     * 指定文件夹下面都是基于时间来生成目录
     * @param string $dirname
     * @return bool|string
     */
    public function storeAs($dirname='upload'){
        $name = $this->hash();
        $time = date('Y-m-d');
        $ext  = $this->getExtension();
        $tmpfile = $this->getRealPath();
        $fileName = $dirname.'/'.$time.'/'.$name.'.'.$ext;
        $filePath = $this->getStaticRoot().$fileName;
        $res = File::createFile($filePath,file_get_contents($tmpfile));
        if($res){
            unset($tmpfile);
            return $fileName;
        }
        return false;
    }

    /**
     * 七牛云上传
     * @return mixed
     * @throws \Exception
     */
    public function qiNiuAs(){
        $name = $this->hash();
        $qiniu = Config::getInstance()->getConf('QINIU');
        $time = date('Y-m-d');
        $ext  = $this->getExtension();
        $upManager = new UploadManager();
        $tmpfile = $this->getRealPath();
        $auth = new Auth((string)$qiniu['access'], (string)$qiniu['secret']);
        $token = $auth->uploadToken((string)$qiniu['bucket']);
        //$error保留了请求响应的信息，失败情况下ret 为none
        list($ret, $error) = $upManager->putFile($token, $time.'/'.$name.'.'.$ext, $tmpfile);
        if($ret){
            unset($tmpfile);
            return $qiniu['domain'].$ret['key'];
        }
        return false;
    }

    /**
     * 删除指定文件
     * @param $filename
     * @return bool
     */
    public function destroy($filename){
        if($this->exists($filename)){
            $filePath = $this->getStaticRoot().$filename;
            return unlink($filePath);
        }
        return false;
    }

    /**
     * 获得指定文件目录或目录文件数组
     * @param string $dirname
     * @return array|bool
     */
    public function scan($dirname='image'){
        $filePath = $this->getStaticRoot().$dirname;
        return File::scanDirectory($filePath);
    }

    /**
     * 判断文件是否存在
     * @param $path
     * @return bool
     */
    public function exists($filename){
        $filePath = $this->getStaticRoot().$filename;
        return file_exists($filePath);
    }

    /**
     * 返回项目静态文件夹的绝对地址
     * @return string
     */
    public function getStaticRoot(){
        $static = Config::getInstance()->getConf('STATIC_ROOT');
        return EASYSWOOLE_ROOT.'/'.$static.'/';
    }

    /**
     * 获取扩展名
     * @return mixed
     */
    public function getExtension(){
        return pathinfo($this->files->getClientFilename(), PATHINFO_EXTENSION);
    }

    /**
     * 临时文件名称
     * @return mixed
     */
    public function getRealPath(){
        return $this->files->getTempName();
    }

    /**
     * 生成hash值
     * @return string
     */
    public function hash(){
        return hash_file('md5',$this->getRealPath());
    }
}
