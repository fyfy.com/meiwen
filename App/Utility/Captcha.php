<?php

namespace App\Utility;

use EasySwoole\EasySwoole\Logger;
use EasySwoole\VerifyCode\Conf;
use EasySwoole\EasySwoole\Config;

class Captcha{

    protected $options;

    public function __construct(){
        $options = Config::getInstance()->getConf('CAPTCHA');
        $this->options = $options;
    }

    /**
     * 创建验证码
     * @return array
     */
    public function create(){
        $config = new Conf();
        $config->setCharset((string)$this->options['charset']);
        $config->setLength($this->options['length']);
        $config->setUseCurve($this->options['line']);
        $config->setUseNoise($this->options['notice']);
        $verify = new \EasySwoole\VerifyCode\VerifyCode($config);
        $obj = $verify->DrawCode();
        $code = $obj->getImageCode();
        $captchaKey = password_hash($code,PASSWORD_DEFAULT);
        $captcha  = $obj->getImageBase64();
        $file  = $obj->getImageFile();
        //删除缓存图片信息
        unlink($file);
        return ['captcha'=>$captcha,'captchaKey'=>$captchaKey];
    }

    /**
     * 验证验证码
     * @param $captcha
     * @param $captchaKey
     * @return bool
     */
    public function check($captcha,$captchaKey){
        if (strlen($captchaKey) === 0) {
            return false;
        }
        return password_verify($captcha,$captchaKey);
    }


}
