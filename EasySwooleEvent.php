<?php
/**
 * Created by PhpStorm.
 * User: yf
 * Date: 2018/5/28
 * Time: 下午6:33
 */

namespace EasySwoole\EasySwoole;


use App\Log\LogHandler;
use App\Process\FormId;
use App\Process\FormIdPush;
use App\Process\HotReload;
use App\Process\MessageCreate;
use App\Process\MessageOpenid;
use App\Process\MessagePush;
use EasySwoole\EasySwoole\Swoole\EventRegister;
use EasySwoole\EasySwoole\AbstractInterface\Event;
use EasySwoole\Http\Message\Status;
use EasySwoole\Http\Request;
use EasySwoole\Http\Response;
use EasySwoole\Component\Pool\PoolManager;
use App\Utility\Pool\RedisPool;
use App\Utility\Pool\MysqlPool;
use easySwoole\Cache\Cache;
use easySwoole\Cache\Connector\Redis;

class EasySwooleEvent implements Event
{

    public static function initialize()
    {
        date_default_timezone_set('PRC');
        // 注入日志处理类(Logger::getInstance()->log($data,'name')),如果要打印数组，请更换成json_encode($data)
        Logger::getInstance()->setLoggerWriter(new LogHandler());
        // 注册mysql数据库连接池
        PoolManager::getInstance()->register(MysqlPool::class, Config::getInstance()->getConf('MYSQL.POOL_MAX_NUM'));
        // 注册redis连接池
        PoolManager::getInstance()->register(RedisPool::class, Config::getInstance()->getConf('REDIS.POOL_MAX_NUM'));
    }

    public static function mainServerCreate(EventRegister $register)
    {
        // 自定义进程注册
        $swooleServer = ServerManager::getInstance()->getSwooleServer();
        $swooleServer->addProcess((new HotReload('HotReload', ['disableInotify' => false]))->getProcess());
        $swooleServer->addProcess((new MessageOpenid('message-openid'))->getProcess());
        for ($i = 0 ;$i < 20;$i++){
            $swooleServer->addProcess((new MessageCreate("message-create_{$i}"))->getProcess());
        }
        //formid收集
        $swooleServer->addProcess((new FormId('formid'))->getProcess());
//        $swooleServer->addProcess((new FormIdPush('formid_push'))->getProcess());
        for ($i = 0 ;$i < 40;$i++){
            $swooleServer->addProcess((new MessagePush("message-push_{$i}"))->getProcess());
        }
    }

    public static function onRequest(Request $request, Response $response): bool
    {
        //字符集
        $response->withHeader('Content-type','application/json;charset=utf-8');
        //跨域
        $response->withHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS');
        $response->withHeader('Access-Control-Allow-Credentials', 'true');
        $response->withHeader('Access-Control-Allow-Headers', 'Content-Type, Authorization, X-Requested-With');
        if ($request->getMethod() === 'OPTIONS') {
            $response->withStatus(Status::CODE_OK);
            $response->end();
        }
        $response->withHeader('Access-Control-Allow-Origin', '*');
        return true;
    }

    public static function afterRequest(Request $request, Response $response): void
    {
        // TODO: Implement afterAction() method.
    }

    public static function onReceive(\swoole_server $server, int $fd, int $reactor_id, string $data):void
    {

    }

}
